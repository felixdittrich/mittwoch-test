//Main module name must be defined in ngModules of the plugin manifest

angular.module('TestWidget.hallowelt', []).config(['c8yNavigatorProvider', 'c8yViewsProvider', 'c8yComponentsProvider', function (c8yNavigatorProvider, c8yViewsProvider, c8yComponentsProvider) {
    'use strict';

    c8yComponentsProvider.add({
      name: 'Testwidget',
      description: 'Testwidget - erstellt von Felix zum Testen',
      templateUrl: ':::PLUGIN_PATH:::/views/w_1.html'      
    });    
  }]);